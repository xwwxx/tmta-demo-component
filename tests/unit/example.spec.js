import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import DemoComponent from "@/components/DemoComponent.vue";

describe("DemoComponent.vue", () => {
  it("do not pass config props", () => {
    const wrapper = shallowMount(DemoComponent);
    expect(wrapper.vm.mergedConfig.msg).to.equal("hi dx");
  });

  it("pass config props", () => {
    const config = {
      msg: "hello world"
    };
    const wrapper = shallowMount(DemoComponent, {
      propsData: { config }
    });
    expect(wrapper.vm.mergedConfig.msg).to.equal(config.msg);
  });
});
