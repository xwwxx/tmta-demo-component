// https://docs.cypress.io/api/introduction/api.html

describe("Demo Component Test", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains(".demo-component", "Hello Daniel");
  });
});
